'use strict';

(function(exports) {
    var NavigationMapDialer = {
        init: function() {
            document.addEventListener('actionListContextSwitched', function(event) {
                NavigationManager.reset(event.detail.selector);
            });
        },
        scrollToElement: function(el, evt) {
          var self = this;
          var checkEdge = self.checkElement(el);
          if (checkEdge.match) {
            if (checkEdge.top)
              el.scrollIntoView(false);
            else
              el.scrollIntoView(true);
          }
        },
        checkElement: function(element) {
          var match = false;
          var top = false;
          var elmStyle = element.style;
          var navId = parseInt(element.dataset.navId);
          var navUpId = parseInt(elmStyle.getPropertyValue('--nav-up'));
          var navDownId = parseInt(elmStyle.getPropertyValue('--nav-down'));
          if (navId <= navUpId || isNaN(navUpId)) {
            match = true;
            top = true;
          } else if (navId >= navDownId || isNaN(navDownId)) {
            match = true;
            top = false;
          }
          return {
            match: match,
            top: top
          }
        }
    };

    var NavigationMapCallLog = {
        _controls: null,
        scrollVar: true,
        navId: undefined,
        latestClassName: undefined,
        latestNavId: undefined,
        menuIsActive: false,

        getFunctionForGenId: function _getFunctionForGenId() {
            var lockalId = 0;
            return function() {
                return lockalId++;
            };
        },

        handleClick: function _handleClick(evt) {
            //costimization of click action.
            var focusedEl = document.querySelector('.focus');
            if (focusedEl == undefined) {
                //same action as in NavHandler
                evt.target.click();
                for (var i = 0; i < evt.target.children.length; i++) {
                    evt.target.children[i].click();
                }
            } else {
                focusedEl.focus();
                focusedEl.click();
            }
        },

        scrollToElement: function _scroll(bestElementToFocus) {
            if (bestElementToFocus.classList.contains('menu-button')) {
              var top = bestElementToFocus.offsetTop
              var parent = bestElementToFocus.parentNode;
              if (top < parent.scrollTop) {
                bestElementToFocus.scrollIntoView(true);
              } else if (top > parent.scrollTop + parent.clientHeight -
                bestElementToFocus.offsetHeight) {
                bestElementToFocus.scrollIntoView(false);
              }
              return;
            }
            //todo scroll for fix doubled header when focused element aligned at bottom
            var callLogContainer = document.getElementById('call-log-container');
            var stickyHeight = document.getElementById('sticky').offsetHeight;
            var elementOffsetBotton = bestElementToFocus.offsetTop + bestElementToFocus.offsetHeight;
            callLogContainer.stickyHiddenGap = bestElementToFocus.offsetHeight;

            if ((bestElementToFocus.offsetTop - stickyHeight) < callLogContainer.scrollTop) {
              callLogContainer.scrollTop = bestElementToFocus.offsetTop - stickyHeight;
            } else if (elementOffsetBotton > (callLogContainer.scrollTop + callLogContainer.offsetHeight)) {
              callLogContainer.scrollTop = elementOffsetBotton - callLogContainer.offsetHeight;
            }
        },

        init: function _init() {
            console.log('init');
            this.navId = this.getFunctionForGenId();

            window.addEventListener("hashchanged", function() {
                NavigationMap.reset();
            }, true);

            window.addEventListener('moz-app-loaded', function(e) {
                NavigationMap.reset("log-item");
            });

            window.addEventListener('keydown-drag-drop-move', function(e) {
                NavigationMap.update();
            });
            window.addEventListener('load', function(e) {
                NavigationMap.reset("log-item");
            });
            document.addEventListener('focusChanged', function(e) {
                var bestElementToFocus = e.detail.focusedElement;
                if (bestElementToFocus == undefined) {
                    return;
                }
                var id = bestElementToFocus.getAttribute('data-nav-id');
                if (NavigationMap.menuIsActive == false) {
                    NavigationMap.latestNavId = id;
                }
            });
            document.addEventListener("menuEvent", function(e) {
                if (e.detail.menuVisible == true) {
                    NavigationMap.switchContext(e.detail.menuName, true);
                } else {
                    NavigationMap.switchContext(undefined, false);
                }
            });
        },

        initNavId: function _initNavId(selector) {
            var items = document.getElementsByClassName(selector);
            for (var j = 0; j < items.length; j++) {
                var navId = items[j].getAttribute('data-nav-id');
                if (navId == undefined) {
                    items[j].setAttribute('data-nav-id', this.navId());
                }
            }
        },

        reset: function _reset(selector, selectedItem) {
            if (CallMenu.optionMenu && CallMenu.optionMenu.form.classList.contains('visible')) {
              return;
            }
            if (selector == undefined) {
                if (this.latestClassName != undefined) {
                    selector = this.latestClassName;
                } else {
                    selector = 'log-item'; //default value
                }
            }
            console.log("NavigationMap reset. selector: " + selector + ', len: ' + document.getElementsByClassName(selector).length + ", selectedItem: " + selectedItem);
            this._controls = document.getElementsByClassName(selector);
            if (this.menuIsActive == false) {
                if (selector != 'focus') {
                    NavigationMap.latestClassName = selector;
                }
            }
            var selectenItemId = 0;
            var focused = document.querySelectorAll(".focus");
            if (focused.length > 0) {
                for (var j = 0; j < focused.length; j++) {
                    focused[j].classList.remove('focus');
                }
            }
            if (this._controls.length == 0) {
                document.dispatchEvent(new CustomEvent("focusChanged", {
                    detail: {
                        focusedElement: undefined
                    }
                }));
                return;
            }
            var item = this._controls[0];
            if (selectedItem != undefined) {
                var el = document.querySelector('[data-nav-id="' + selectedItem + '"]');
                if (el != undefined) {
                    item = el;
                }
            }
            this.setFocus(item);
            this.update(selector);
        },

        /*option menu*/
        reset_options: function _reset() {
          NavigationMap._controls =
            document.querySelectorAll('#mainmenu .menu-button.p-pri');
          NavigationMap.reset_controls();
          var focused = document.querySelectorAll('.focus');
          for (var i = 0; i < focused.length; i++) {
            focused[i].classList.remove('focus');
          }
          if (NavigationMap._controls.length) {
            NavigationMap._controls[0].classList.add('focus');
            NavigationMap._controls[0].focus();
          }
        },

        update: function _update(selector) {
            if (document.querySelectorAll(".focus").length < 1) {
                this.reset();
                return;
            }
            selector = selector || NavigationMap.latestClassName || 'log-item';
            this.initNavId(selector);
            var i = 0;

            var LogItems = document.getElementsByClassName(selector);
            if (LogItems.length > 0) {
                for (var j = 0; j < LogItems.length; j++) {
                    var LogItem = LogItems[j];
                    var prevItem = LogItems[j - 1] || LogItems[LogItems.length - 1];
                    var nextItem = LogItems[j + 1] || LogItems[0];
                    LogItem.style.setProperty('--nav-down', nextItem.getAttribute('data-nav-id'));
                    LogItem.style.setProperty('--nav-up', prevItem.getAttribute('data-nav-id'));
                }
            }
        },

        reset_controls: function() {
          var _controls = this._controls;
          if (_controls.length) {
            var i = 0;
            var id = this.navId();
            var prevId = id;
            var nextId = this.navId();

            for (i = 0; i < _controls.length; i++) {

              _controls[i].style.setProperty('--nav-left',  -1);
              _controls[i].style.setProperty('--nav-right', -1);
              _controls[i].setAttribute('tabindex', 0);
              _controls[i].setAttribute('data-nav-id', id);

              _controls[i].style.setProperty('--nav-down', nextId);
              _controls[i].style.setProperty('--nav-up',   prevId);

              prevId = id;
              id = nextId;
              nextId = this.navId();
            }

            var lastCtrl  = _controls[_controls.length - 1];
            var firstCtrl = _controls[0];

            lastCtrl.style.setProperty('--nav-down', firstCtrl.getAttribute('data-nav-id'));
            firstCtrl.style.setProperty('--nav-up',  lastCtrl.getAttribute('data-nav-id'));
          }
        },

        setFocus: function _setFocus(item) {
            item.setAttribute('tabindex', 1);
            item.classList.add('focus');
            item.focus();
            this.scrollToElement(item);
            document.dispatchEvent(new CustomEvent("focusChanged", {
                detail: {
                    focusedElement: item
                }
            }));
        },

        switchContext: function _switchContext(itemClassName, menu) {
            console.log("NavigationMap switchContext itemClassName: " + itemClassName + ", latestClassName: " + NavigationMap.latestClassName + ", latestNavId:" + NavigationMap.latestNavId);
            NavigationMap.menuIsActive = false;
            if (menu != undefined) {
                NavigationMap.menuIsActive = menu;
            }
            if (itemClassName != undefined) {
                this.reset(itemClassName);
            } else {
              if (OptionHelper.CallInfoVisible == false || OptionHelper.CallInfoVisible == undefined) {
                this.reset(NavigationMap.latestClassName, NavigationMap.latestNavId);
              }
            }
        }
    };

    var getNavigationMap = function() {
        var destination = window.location.hash;
        switch (destination) {
            case '#call-log-view':
                return NavigationMapCallLog;
        }
    };

    var NavigationMap = getNavigationMap();
    console.log(NavigationMap);
    exports.NavigationMap = getNavigationMap();
})(window);
