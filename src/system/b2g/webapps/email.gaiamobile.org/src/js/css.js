define({
    load: function(e, t, n, o) {
        if (o.isBuild) return n();
        var r = document.createElement("link");
        r.type = "text/css", r.rel = "stylesheet", r.href = t.toUrl(e + ".css"), r.addEventListener("load", n, !1), 
        document.head.appendChild(r);
    }
});