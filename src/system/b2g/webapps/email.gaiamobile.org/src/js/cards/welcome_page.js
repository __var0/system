define([ "require", "exports", "module", "cards", "html_cache", "./base", "template!./welcome_page.html" ], function(e, t, n) {
    var o, r = e("cards"), i = e("html_cache");
    return [ e("./base")(e("template!./welcome_page.html")), {
        createdCallback: function() {
            i.cloneAndSave(n.id, this);
        },
        onArgs: function() {
            o = this;
        },
        onCardVisible: function() {
            var e = [ {
                name: "Next",
                l10nId: "next",
                priority: 3,
                method: function() {
                    o.onNext();
                }
            } ];
            NavigationMap.setSoftKeyBar(e);
            var t = document.getElementsByTagName("cards-welcome-page")[0];
            t.setAttribute("role", "heading"), t.setAttribute("aria-labelledby", "welcome-header"), 
            this.msgNode.focus();
        },
        onNext: function() {
            r.pushCard("setup_account_info", "animate");
        },
        die: function() {}
    } ];
});