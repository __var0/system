var Redirect = function() {
    var e = function() {
        var e = window.location.href, t = {
            type: "oauth2Complete",
            data: {}
        }, n = e.split("?")[1] || "";
        if (n.length > 0) {
            var o = n.split("&");
            o.forEach(function(e) {
                var n = e.split("=");
                t.data[decodeURIComponent(n[0])] = decodeURIComponent(n[1]);
            }), window.opener.postMessage(t, window.location.origin);
        }
        navigator.mozApps.getSelf().onsuccess = function(e) {
            var t = e.target.result;
            t && t.clearBrowserData();
        }, window.close();
    };
    return {
        init: e
    };
}();

Redirect.init();