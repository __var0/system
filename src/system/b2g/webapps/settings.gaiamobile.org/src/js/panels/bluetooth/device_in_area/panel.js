/**
 * The Bluetooth panel
 */
define(['require','modules/bluetooth/bluetooth_context','modules/bluetooth/bluetooth_connection_manager','panels/bluetooth/bt_template_factory','modules/mvvm/list_view','modules/settings_panel','modules/settings_service','shared/toaster'],function(require) {
  

  var BtContext = require('modules/bluetooth/bluetooth_context');
  var BtConnectionManager =
    require('modules/bluetooth/bluetooth_connection_manager');
  var BtTemplateFactory = require('panels/bluetooth/bt_template_factory');
  var ListView = require('modules/mvvm/list_view');
  var SettingsPanel = require('modules/settings_panel');
  var SettingsService = require('modules/settings_service');
  var Toaster = require('shared/toaster');

  var _debug = false;
  var debug = function() {};
  if (_debug) {
    debug = function btp_debug(msg) {
      console.log('--> [Bluetooth][Panel]: ' + msg);
    };
  }

  return function ctor_bluetooth() {
    var elements;
    var searchingBar;
    var searchCompleteBar;
    var noDevicesBar;
    let pairingBar;

    return SettingsPanel({
      onInit: function(panel) {
        // To record current bluetooth status
        this.status = 'init';
        debug('Init bluetooth nearby devices.');
        this._boundUpdateSearchingStatus = this._updateSearchingStatus.bind(this);
        this._boundUpdateSearchingSoftkey = this._updateSearchingSoftkey.bind(this);

        elements = {
          panel: panel,
          foundDevicesList: panel.querySelector('#bluetooth-devices'),
          searchingItem: panel.querySelector('#bluetooth-searching'),
          nearbyDevicesDesc: panel.querySelector('#nodevices-nearby'),
        };
        let cskSelect = {
          name: 'select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        };
        let rskRescan = {
          name: 'Rescan',
          l10nId: 'rescan',
          priority: 3,
          method: this._searchAgain
        };
        searchingBar = {items: [cskSelect]};
        searchCompleteBar = {items: [cskSelect, rskRescan]};
        noDevicesBar = {items: [rskRescan]};
        pairingBar = { items: [cskSelect] };

        // found devices list item click events
        let foundDeviceTemplate =
          BtTemplateFactory('remote', this._onFoundDeviceItemClick.bind(this));

        // create found devices list view
        ListView(elements.foundDevicesList,
                 BtContext.getRemoteDevices(),
                 foundDeviceTemplate);
      },

      onBeforeShow: function() {
        debug('onBeforeShow():');
        // If we still on pairing with remote device,
        // we should not to reload current page again.
        if ('pairing' === this.status) {
          return;
        }

        BtContext.observe('discovering', this._boundUpdateSearchingStatus);
        BtContext.observe('hasFoundDevice', this._boundUpdateSearchingSoftkey);
      },

      onShow: function() {
        debug('onShow():');
        if ('pairing' === this.status) {
          return;
        }

        // update the current bluetooth status
        this.status = 'update'
        this._searchAgain();
      },

      onBeforeHide: function() {
        debug('onBeforeHide():');
        BtContext.unobserve('discovering', this._boundUpdateSearchingStatus);
        BtContext.unobserve('hasFoundDevice', this._boundUpdateSearchingSoftkey);
        SettingsSoftkey.hide();
      },

      onHide: function() {
        debug('onHide():');
        BtContext.stopDiscovery();
      },

      _onFoundDeviceItemClick: function(deviceItem) {
        this._toPairDevice(deviceItem);
      },

      _toPairDevice: function(deviceItem) {
        debug('_onFoundDeviceItemClick(): deviceItem.address = ' +
              deviceItem.address);

        // Update device pairing status first.
        deviceItem.paired = 'pairing';
        this.status = 'pairing';

        // Pair with the remote device.
        BtContext.pair(deviceItem.address).then(() => {
          this._showToast(deviceItem);
          debug('paired with ' + deviceItem.name + ' success.');

          // Connect the device which is just paired.
          this._connectHeadsetDevice(deviceItem);

          // Reload current nearby devices page
          this.status = 'paired';
          this.onBeforeShow();
          this.onShow();

          this._updateSoftkey(searchCompleteBar);

        }, (reason) => {
          debug('_onFoundDeviceItemClick(): pair failed, ' +
                'reason = ' + reason);
          // Reset the paired status back to false,
          // since the 'pairing' status is given in Gaia side.
          deviceItem.paired = false;
          this.status = 'pairFailed';
          this._showConfirmDialog(deviceItem);
          this._updateSoftkey(searchCompleteBar);
        });
      },

      _connectHeadsetDevice: function(deviceItem) {
        if (!((deviceItem.type === 'audio-card') ||
              (deviceItem.type === 'audio-input-microphone'))) {
          return;
        }

        BtConnectionManager.connect(deviceItem.data).then(() => {
          debug('_connectHeadsetDevice(): connect device successfully');
        }, (reason) => {
          debug('_connectHeadsetDevice(): connect device failed, ' +
                'reason = ' + reason);
        });
      },

      _updateSearchingStatus: function(discovering) {
        debug('_updateSearchingItem(): ' +
              'callback from observe "discovering" = ' + discovering);
        elements.searchingItem.hidden = !discovering;
        var devicesNearby = BtContext.getRemoteDevices();
        if (discovering === false) {
          if (devicesNearby.length !== 0) {
            if (this.status === 'pairing') {
              this._updateSoftkey(pairingBar);
            } else {
              this._updateSoftkey(searchCompleteBar);
            }
          } else {
            elements.nearbyDevicesDesc.classList.add('visible');
            this._updateSoftkey(noDevicesBar);
          }
        }
      },

      _updateSearchingSoftkey: function(hasFoundDevice) {
        if (hasFoundDevice === true) {
          this._updateSoftkey(searchingBar);
        } else{
          SettingsSoftkey.hide();
        }
      },

      _searchAgain: function() {
        SettingsSoftkey.hide();
        elements.nearbyDevicesDesc.classList.remove('visible');
        BtContext.startDiscovery().then(() => {
          debug('_searchAgain(): startDiscovery successfully');
        }, (reason) => {
          debug('_searchAgain(): startDiscovery failed, ' +
                'reason = ' + reason);
        });
      },

      _showConfirmDialog: function(deviceItem) {
        var self = this;
        var dialogConfig = {
          title: {id: 'error-pair-title', args: {}},
          body: {id: 'error-pair-fail', args: {'devicename':deviceItem.name}},
          desc:{id: 'error-pair-checkpin',args:{}},
          cancel: {
            l10nId:'cancel',
            priority:1,
            callback: function() {}
          },
          confirm: {
            l10nId:'pair',
            priority:3,
            callback: function() {
              self._toPairDevice(deviceItem);
            }
          }
        }
        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('app-confirmation-dialog'));
      },

      _showToast: function(device) {
        var toast = {
          messageL10nId: 'paried-with-device',
          messageL10nArgs: {'deviceName': device.name},
          latency: 2000,
          useTransition: true
        };
        Toaster.showToast(toast);
      },

      _updateSoftkey: function(params) {
        SettingsSoftkey.init(params);
        SettingsSoftkey.show();
      }
    });
  };
});
