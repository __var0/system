/**
 * Handle factory reset functionality
 *
 * @module about/FactoryReset
 */
define(['require','modules/settings_service','shared/settings_listener'],function(require) {
  

  var SettingsService = require('modules/settings_service');
  var SettingsListener = require('shared/settings_listener');

  /**
   * @alias module:about/FactoryReset
   * @class FactoryReset
   * @returns {FactoryReset}
   */
  var skCancel = {
    name: 'Cancel',
    l10nId: 'cancel',
    priority: 1,
    method: null
  };
  var skReset = {
    name: 'Reset',
    l10nId: 'reset',
    priority: 3,
    method: null
  };
  var resetSoftKeyBar = {
    header: {
      l10nId: 'message'
    },
    items: [skCancel, skReset]
  };
  var focusedElement = null;
  var FactoryReset = function() {
    this._elements = null;
  };
  var _handleKeyDown = function(e) {
    switch (e.key) {
      case 'BrowserBack':
      case 'Backspace':
      case 'KanjiMode':
        var aboutInfo = document.getElementById('about-deviceInfo');
        var header = document.getElementById('adbout-header');
        var resetDialog = document.getElementById('about-confirm-dialog');
        var head1 = header.firstElementChild;
        head1.setAttribute('data-l10n-id', 'deviceInfo-header');
        skReset.onclick = null;
        skCancel.onclick = null;
        aboutInfo.hidden = false;
        resetDialog.hidden = true;
        SettingsSoftkey.hide();
        window.removeEventListener('keydown', _handleKeyDown);
        focusedElement.classList.add('focus');
        break;

    }
  };

  var antitheftEnabled = false;

  function setAntitheftEnabled(value) {
    antitheftEnabled = value;
  }

  FactoryReset.prototype = {
    /**
     * initialization
     *
     * @access public
     * @memberOf FactoryReset.prototype
     * @param {HTMLElement} elements
     */
    init: function fr_init(elements) {
      this._elements = elements;
      var self = this;
      if (navigator.mozPower) {
        this._elements.resetButton.disabled = false;
        this._elements.resetButton.addEventListener('click',
          this._resetClick.bind(this));
        window.addEventListener('show-about-dialog', function(e) {
          self._resetClick();
        });
      } else {
        // disable button if mozPower is undefined or can't be used
        this._elements.resetButton.disabled = true;
      }

      SettingsListener.observe('antitheft.enabled', false, setAntitheftEnabled);
    },

    /**
     * click handler.
     *
     * @access private
     * @memberOf FactoryReset.prototype
     */
    _resetClick: function fr__resetClick() {
      var aboutInfo = document.getElementById('about-deviceInfo');
      var resetDialog = document.getElementById('about-confirm-dialog');
      var header = document.getElementById('adbout-header');
      var head1 = header.firstElementChild;
      var self = this;
      focusedElement = self._elements.resetButton.parentNode;
      focusedElement.classList.remove('focus');
      head1.setAttribute('data-l10n-id', 'reset-warning-title');
      SettingsSoftkey.init(resetSoftKeyBar);
      SettingsSoftkey.show();
      aboutInfo.hidden = true;
      resetDialog.hidden = false;
      window.addEventListener('keydown', _handleKeyDown);
      skCancel.method = function fr_lconfirmButton() {
        head1.setAttribute('data-l10n-id', 'deviceInfo-header');
        skCancel.onclick = null;
        aboutInfo.hidden = false;
        resetDialog.hidden = true;
        SettingsSoftkey.hide();
        window.removeEventListener('keydown', _handleKeyDown);
        focusedElement.classList.add('focus');
        SettingsListener.unobserve('antitheft.enabled', setAntitheftEnabled);
      };
      skReset.method = function fr_RconfirmButton() {
        head1.setAttribute('data-l10n-id', 'deviceInfo-header');
        skReset.onclick = null;
        aboutInfo.hidden = false;
        resetDialog.hidden = true;
        SettingsSoftkey.hide();
        window.removeEventListener('keydown', _handleKeyDown);

        FxAccountsIACHelper.getAccounts(accts => {
          //get Account
          var email = accts && (accts.email || accts.accountId);
          if (email === '' || !antitheftEnabled) {
            self._factoryReset();
          } else {
            SettingsService.navigate('antitheft-disable', {
              enabled: true,
              dataHref: '#about',
              descriptions: 'reset-disable-antitheft-warning'
            });
          }

          SettingsListener.unobserve('antitheft.enabled', setAntitheftEnabled);
        });
      };
    },

    /**
     * call mozPower API to reset device
     *
     * @access private
     * @memberOf FactoryReset.prototype
     */
    _factoryReset: function fr__factoryReset() {
      var power = navigator.mozPower;
      if (!power) {
        console.error('Cannot get mozPower');
        return;
      }

      if (!power.factoryReset) {
        console.error('Cannot invoke mozPower.factoryReset()');
        return;
      }

      power.factoryReset();
    }
  };

  return function ctor_factoryReset() {
    return new FactoryReset();
  };
});
