'use strict';

/**
 * FMRadioNotifyWidget handle iac-FMRadioComms message
 * from FM Radio app to display, update, or hide notification.
 * @class FMRadioNotifyWidget
 * @param {} container widget container
 */
function FMRadioNotifyWidget(container) {
  this.container = container;
  this.contentFirst = document.getElementById('fm-radio-content-first');
  this.contentSecond = document.getElementById('fm-radio-content-second');
  this.origin = 'app://fm.gaiamobile.org';
  this.updateWidget = this.updateWidget.bind(this);
  window.addEventListener('iac-FMRadioComms', this.onMessage.bind(this));
  window.addEventListener('appterminated', function(event) {
    if (event.detail.origin === this.origin) {
      this.hidden = true;
    }
  }.bind(this));
}

FMRadioNotifyWidget.prototype = {
  get hidden() {
    return this.container.classList.contains('hidden');
  },

  set hidden(value) {
    this.container.classList.toggle('hidden', value);
  },

  updateWidget: function _updateWidget(message) {
    if (message.name === '') {
      this.contentFirst.textContent = message.freq;
      this.contentFirst.classList.add('fmradio-content-suffix');
      this.contentSecond.textContent = '';
    } else {
      this.contentFirst.textContent = message.name;
      this.contentFirst.classList.remove('fmradio-content-suffix');
      this.contentSecond.textContent = message.freq + 'MHZ';
    }
  },

  onMessage: function _onMessage(e) {
    var message = e.detail;
    switch (message.action) {
      case 'update':
        if (!this.hidden) {
          this.updateWidget(message);
        }
        break;
      case 'show':
        this.hidden = false;
        break;
      case 'hide':
        this.hidden = true;
        break;
      default:
        break;
    }
  },

  openFMRadio: function _openFMRadio(event) {
    if (this.origin) {
      var evt = new CustomEvent('displayapp', {
        bubbles: true,
        cancelable: true,
        detail: appWindowManager.getApp(this.origin)
      });
      window.dispatchEvent(evt);
    }
  }
};
