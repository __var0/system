'use strict';(function(exports){var operators;const NATIONAL_ECC_LIST={"228":["117","118","143","144","145","147"],"466":["110","119"],"460":["110","119","120"],"450":["111","112","113","117","118","119","122","125"]};function init(){let conns=navigator.mozMobileConnections;operators=Array.from(conns).map(function(item){let iccObj=navigator.mozIccManager.getIccById(item.iccId);if(!iccObj){return null;}
let iccInfo=iccObj.iccInfo;return iccInfo?iccInfo.mcc:null;});console.log('national_ecc_list current operators = '+operators);}
function contains(arr,obj){if(!arr)return false;var i=arr.length;while(i--){if(arr[i]===obj){return true;}}
return false;}
function isFromNationalEcc(number,index){let ret=contains(NATIONAL_ECC_LIST[operators[index]],number);console.log('national_ecc_list ret = '+ret);return ret;}
var NationalEccList={init:init,isFromNationalEcc:isFromNationalEcc};exports.NationalEccList=NationalEccList;})(window);