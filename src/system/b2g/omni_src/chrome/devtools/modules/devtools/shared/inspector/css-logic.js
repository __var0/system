"use strict";const{Cc,Ci,Cu}=require("chrome");const Services=require("Services");const DevToolsUtils=require("devtools/shared/DevToolsUtils");const{getRootBindingParent}=require("devtools/shared/layout/utils");
loader.lazyRequireGetter(this,"CSS","CSS");function CssLogic(){this._propertyInfos={};}
exports.CssLogic=CssLogic;CssLogic.FILTER={USER:"user", UA:"ua",};CssLogic.MEDIA={ALL:"all",SCREEN:"screen",};CssLogic.STATUS={BEST:3,MATCHED:2,PARENT_MATCH:1,UNMATCHED:0,UNKNOWN:-1,};CssLogic.prototype={viewedElement:null,viewedDocument:null,_sheets:null,_sheetsCached:false,_ruleCount:0,_computedStyle:null, _sourceFilter:CssLogic.FILTER.USER,
_passId:0,_matchId:0,_matchedRules:null,_matchedSelectors:null, _keyframesRules:null,reset:function(){this._propertyInfos={};this._ruleCount=0;this._sheetIndex=0;this._sheets={};this._sheetsCached=false;this._matchedRules=null;this._matchedSelectors=null;this._keyframesRules=[];},highlight:function(viewedElement){if(!viewedElement){this.viewedElement=null;this.viewedDocument=null;this._computedStyle=null;this.reset();return;}
if(viewedElement===this.viewedElement){return;}
this.viewedElement=viewedElement;let doc=this.viewedElement.ownerDocument;if(doc!=this.viewedDocument){this.viewedDocument=doc;this._cacheSheets();}else{this._propertyInfos={};}
this._matchedRules=null;this._matchedSelectors=null;this._computedStyle=CssLogic.getComputedStyle(this.viewedElement);},get computedStyle(){return this._computedStyle;},get sourceFilter(){return this._sourceFilter;},set sourceFilter(value){let oldValue=this._sourceFilter;this._sourceFilter=value;let ruleCount=0;this.forEachSheet(function(sheet){sheet._sheetAllowed=-1;if(sheet.contentSheet&&sheet.sheetAllowed){ruleCount+=sheet.ruleCount;}},this);this._ruleCount=ruleCount;
let needFullUpdate=(oldValue==CssLogic.FILTER.UA||value==CssLogic.FILTER.UA);if(needFullUpdate){this._matchedRules=null;this._matchedSelectors=null;this._propertyInfos={};}else{for(let property in this._propertyInfos){this._propertyInfos[property].needRefilter=true;}}},getPropertyInfo:function(property){if(!this.viewedElement){return{};}
let info=this._propertyInfos[property];if(!info){info=new CssPropertyInfo(this,property);this._propertyInfos[property]=info;}
return info;},_cacheSheets:function(){this._passId++;this.reset(); Array.prototype.forEach.call(this.viewedDocument.styleSheets,this._cacheSheet,this);this._sheetsCached=true;},_cacheSheet:function(domSheet){if(domSheet.disabled){return;}
if(!this.mediaMatches(domSheet)){return;}
let cssSheet=this.getSheet(domSheet,this._sheetIndex++);if(cssSheet._passId!=this._passId){cssSheet._passId=this._passId;for(let aDomRule of domSheet.cssRules){if(aDomRule.type==Ci.nsIDOMCSSRule.IMPORT_RULE&&aDomRule.styleSheet&&this.mediaMatches(aDomRule)){this._cacheSheet(aDomRule.styleSheet);}else if(aDomRule.type==Ci.nsIDOMCSSRule.KEYFRAMES_RULE){this._keyframesRules.push(aDomRule);}}}},get sheets(){if(!this._sheetsCached){this._cacheSheets();}
let sheets=[];this.forEachSheet(function(sheet){if(sheet.contentSheet){sheets.push(sheet);}},this);return sheets;},get keyframesRules(){if(!this._sheetsCached){this._cacheSheets();}
return this._keyframesRules;},getSheet:function(domSheet,index){let cacheId="";if(domSheet.href){cacheId=domSheet.href;}else if(domSheet.ownerNode&&domSheet.ownerNode.ownerDocument){cacheId=domSheet.ownerNode.ownerDocument.location;}
let sheet=null;let sheetFound=false;if(cacheId in this._sheets){for(let i=0,numSheets=this._sheets[cacheId].length;i<numSheets;i++){sheet=this._sheets[cacheId][i];if(sheet.domSheet===domSheet){if(index!=-1){sheet.index=index;}
sheetFound=true;break;}}}
if(!sheetFound){if(!(cacheId in this._sheets)){this._sheets[cacheId]=[];}
sheet=new CssSheet(this,domSheet,index);if(sheet.sheetAllowed&&sheet.contentSheet){this._ruleCount+=sheet.ruleCount;}
this._sheets[cacheId].push(sheet);}
return sheet;},forEachSheet:function(callback,scope){for(let cacheId in this._sheets){let sheets=this._sheets[cacheId];for(let i=0;i<sheets.length;i++){ try{let sheet=sheets[i];
sheet.domSheet;callback.call(scope,sheet,i,sheets);}catch(e){sheets.splice(i,1);i--;}}}},forSomeSheets:function(callback,scope){for(let cacheId in this._sheets){if(this._sheets[cacheId].some(callback,scope)){return true;}}
return false;},get ruleCount(){if(!this._sheetsCached){this._cacheSheets();}
return this._ruleCount;},processMatchedSelectors:function(callback,scope){if(this._matchedSelectors){if(callback){this._passId++;this._matchedSelectors.forEach(function(value){callback.call(scope,value[0],value[1]);value[0].cssRule._passId=this._passId;},this);}
return;}
if(!this._matchedRules){this._buildMatchedRules();}
this._matchedSelectors=[];this._passId++;for(let i=0;i<this._matchedRules.length;i++){let rule=this._matchedRules[i][0];let status=this._matchedRules[i][1];rule.selectors.forEach(function(selector){if(selector._matchId!==this._matchId&&(selector.elementStyle||this.selectorMatchesElement(rule.domRule,selector.selectorIndex))){selector._matchId=this._matchId;this._matchedSelectors.push([selector,status]);if(callback){callback.call(scope,selector,status);}}},this);rule._passId=this._passId;}},selectorMatchesElement:function(domRule,idx){let element=this.viewedElement;do{if(domUtils.selectorMatchesElement(element,domRule,idx)){return true;}}while((element=element.parentNode)&&element.nodeType===Ci.nsIDOMNode.ELEMENT_NODE);return false;},hasMatchedSelectors:function(properties){if(!this._matchedRules){this._buildMatchedRules();}
let result={};this._matchedRules.some(function(value){let rule=value[0];let status=value[1];properties=properties.filter((property)=>{
if(rule.getPropertyValue(property)&&(status==CssLogic.STATUS.MATCHED||(status==CssLogic.STATUS.PARENT_MATCH&&domUtils.isInheritedProperty(property)))){result[property]=true;return false;}
return true;});return properties.length==0;},this);return result;},_buildMatchedRules:function(){let domRules;let element=this.viewedElement;let filter=this.sourceFilter;let sheetIndex=0;this._matchId++;this._passId++;this._matchedRules=[];if(!element){return;}
do{let status=this.viewedElement===element?CssLogic.STATUS.MATCHED:CssLogic.STATUS.PARENT_MATCH;try{
let{bindingElement,pseudo}=CssLogic.getBindingElementAndPseudo(element);domRules=domUtils.getCSSStyleRules(bindingElement,pseudo);}catch(ex){Services.console.logStringMessage("CL__buildMatchedRules error: "+ex);continue;}
let numDomRules=domRules?domRules.Count():0;for(let i=0;i<numDomRules;i++){let domRule=domRules.GetElementAt(i);if(domRule.type!==Ci.nsIDOMCSSRule.STYLE_RULE){continue;}
let sheet=this.getSheet(domRule.parentStyleSheet,-1);if(sheet._passId!==this._passId){sheet.index=sheetIndex++;sheet._passId=this._passId;}
if(filter===CssLogic.FILTER.USER&&!sheet.contentSheet){continue;}
let rule=sheet.getRule(domRule);if(rule._passId===this._passId){continue;}
rule._matchId=this._matchId;rule._passId=this._passId;this._matchedRules.push([rule,status]);}
if(element.style&&element.style.length>0){let rule=new CssRule(null,{style:element.style},element);rule._matchId=this._matchId;rule._passId=this._passId;this._matchedRules.push([rule,status]);}}while((element=element.parentNode)&&element.nodeType===Ci.nsIDOMNode.ELEMENT_NODE);},mediaMatches:function(domObject){let mediaText=domObject.media.mediaText;return!mediaText||this.viewedDocument.defaultView.matchMedia(mediaText).matches;},};CssLogic.getShortName=function(element){if(!element){return"null";}
if(element.id){return"#"+element.id;}
let priorSiblings=0;let temp=element;while((temp=temp.previousElementSibling)){priorSiblings++;}
return element.tagName+"["+priorSiblings+"]";};CssLogic.getShortNamePath=function(element){let doc=element.ownerDocument;let reply=[];if(!element){return reply;}

do{reply.unshift({display:CssLogic.getShortName(element),element:element});element=element.parentNode;}while(element&&element!=doc.body&&element!=doc.head&&element!=doc);return reply;};CssLogic.getSelectors=function(domRule){let selectors=[];let len=domUtils.getSelectorCount(domRule);for(let i=0;i<len;i++){let text=domUtils.getSelectorText(domRule,i);selectors.push(text);}
return selectors;};CssLogic.getBindingElementAndPseudo=function(node){let bindingElement=node;let pseudo=null;if(node.nodeName=="_moz_generated_content_before"){bindingElement=node.parentNode;pseudo=":before";}else if(node.nodeName=="_moz_generated_content_after"){bindingElement=node.parentNode;pseudo=":after";}
return{bindingElement:bindingElement,pseudo:pseudo};};CssLogic.getComputedStyle=function(node){if(!node||Cu.isDeadWrapper(node)||node.nodeType!==Ci.nsIDOMNode.ELEMENT_NODE||!node.ownerDocument||!node.ownerDocument.defaultView){return null;}
let{bindingElement,pseudo}=CssLogic.getBindingElementAndPseudo(node);return node.ownerDocument.defaultView.getComputedStyle(bindingElement,pseudo);};CssLogic.l10n=function(name){return CssLogic._strings.GetStringFromName(name);};DevToolsUtils.defineLazyGetter(CssLogic,"_strings",function(){return Services.strings.createBundle("chrome://devtools-shared/locale/styleinspector.properties");});CssLogic.isContentStylesheet=function(sheet){return sheet.parsingMode!=="agent";};CssLogic.href=function(sheet){let href=sheet.href;if(!href){href=sheet.ownerNode.ownerDocument.location;}
return href;};CssLogic.shortSource=function(sheet){ if(!sheet||!sheet.href){return CssLogic.l10n("rule.sourceInline");} 
let url={};try{url=Services.io.newURI(sheet.href,null,null);url=url.QueryInterface(Ci.nsIURL);}catch(ex){}
if(url.fileName){return url.fileName;}
if(url.filePath){return url.filePath;}
if(url.query){return url.query;}
let dataUrl=sheet.href.match(/^(data:[^,]*),/);return dataUrl?dataUrl[1]:sheet.href;};function positionInNodeList(element,nodeList){for(let i=0;i<nodeList.length;i++){if(element===nodeList[i]){return i;}}
return-1;}
CssLogic.findCssSelector=function(ele){ele=getRootBindingParent(ele);let document=ele.ownerDocument;if(!document||!document.contains(ele)){throw new Error("findCssSelector received element not inside document");} 
if(ele.id&&document.querySelectorAll("#"+CSS.escape(ele.id)).length===1){return"#"+CSS.escape(ele.id);} 
let tagName=ele.localName;if(tagName==="html"){return"html";}
if(tagName==="head"){return"head";}
if(tagName==="body"){return"body";} 
let selector,index,matches;if(ele.classList.length>0){for(let i=0;i<ele.classList.length;i++){selector="."+CSS.escape(ele.classList.item(i));matches=document.querySelectorAll(selector);if(matches.length===1){return selector;}
selector=tagName+selector;matches=document.querySelectorAll(selector);if(matches.length===1){return selector;} 
index=positionInNodeList(ele,ele.parentNode.children)+1;selector=selector+":nth-child("+index+")";matches=document.querySelectorAll(selector);if(matches.length===1){return selector;}}}
if(ele.parentNode!==document){index=positionInNodeList(ele,ele.parentNode.children)+1;selector=CssLogic.findCssSelector(ele.parentNode)+" > "+
tagName+":nth-child("+index+")";}
return selector;};const TAB_CHARS="\t";CssLogic.prettifyCSS=function(text,ruleCount){if(CssLogic.LINE_SEPARATOR==null){let os=Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime).OS;CssLogic.LINE_SEPARATOR=(os==="WINNT"?"\r\n":"\n");} 
text=text.replace(/(?:^\s*<!--[\r\n]*)|(?:\s*-->\s*$)/g,"");let originalText=text;text=text.trim();let lineCount=text.split("\n").length-1;if(ruleCount!==null&&lineCount>=ruleCount){return originalText;}





let indent="";let indentLevel=0;let tokens=domUtils.getCSSLexer(text);let result="";let pushbackToken=undefined;



let readUntilSignificantToken=()=>{while(true){let token=tokens.nextToken();if(!token||token.tokenType!=="whitespace"){pushbackToken=token;return token;}

let nextToken=tokens.nextToken();if(!nextToken||nextToken.tokenType!=="comment"){pushbackToken=nextToken;return token;}
result=result+text.substring(token.startOffset,nextToken.endOffset);}};let startIndex;let endIndex;let anyNonWS;let isCloseBrace;
let lastWasWS;


let readUntilNewlineNeeded=()=>{let token;while(true){if(pushbackToken){token=pushbackToken;pushbackToken=undefined;}else{token=tokens.nextToken();}
if(!token){endIndex=text.length;break;}

if(token.tokenType==="symbol"&&token.text==="}"){isCloseBrace=true;break;}else if(token.tokenType==="symbol"&&token.text==="{"){break;}
if(token.tokenType!=="whitespace"){anyNonWS=true;}
if(startIndex===undefined){startIndex=token.startOffset;}
endIndex=token.endOffset;if(token.tokenType==="symbol"&&token.text===";"){break;}
lastWasWS=token.tokenType==="whitespace";}
return token;};while(true){startIndex=undefined;endIndex=undefined;anyNonWS=false;isCloseBrace=false;lastWasWS=false;let token=readUntilNewlineNeeded();if(startIndex!==undefined){if(isCloseBrace&&!anyNonWS){
}else{result=result+indent+text.substring(startIndex,endIndex);if(isCloseBrace){result+=CssLogic.LINE_SEPARATOR;}}}
if(isCloseBrace){indent=TAB_CHARS.repeat(--indentLevel);result=result+indent+"}";}
if(!token){break;}
if(token.tokenType==="symbol"&&token.text==="{"){if(!lastWasWS){result+=" ";}
result+="{";indent=TAB_CHARS.repeat(++indentLevel);}

token=readUntilSignificantToken();
if(pushbackToken&&token&&token.tokenType==="whitespace"&&/\n/g.test(text.substring(token.startOffset,token.endOffset))){return originalText;}
result=result+CssLogic.LINE_SEPARATOR;if(!pushbackToken){break;}}
return result;};function CssSheet(cssLogic,domSheet,index){this._cssLogic=cssLogic;this.domSheet=domSheet;this.index=this.contentSheet?index:-100*index;this._href=null;this._shortSource=null;this._sheetAllowed=null;this._rules={};this._ruleCount=-1;}
CssSheet.prototype={_passId:null,_contentSheet:null,_mediaMatches:null,get contentSheet(){if(this._contentSheet===null){this._contentSheet=CssLogic.isContentStylesheet(this.domSheet);}
return this._contentSheet;},get disabled(){return this.domSheet.disabled;},get mediaMatches(){if(this._mediaMatches===null){this._mediaMatches=this._cssLogic.mediaMatches(this.domSheet);}
return this._mediaMatches;},get href(){if(this._href){return this._href;}
this._href=CssLogic.href(this.domSheet);return this._href;},get shortSource(){if(this._shortSource){return this._shortSource;}
this._shortSource=CssLogic.shortSource(this.domSheet);return this._shortSource;},get sheetAllowed(){if(this._sheetAllowed!==null){return this._sheetAllowed;}
this._sheetAllowed=true;let filter=this._cssLogic.sourceFilter;if(filter===CssLogic.FILTER.USER&&!this.contentSheet){this._sheetAllowed=false;}
if(filter!==CssLogic.FILTER.USER&&filter!==CssLogic.FILTER.UA){this._sheetAllowed=(filter===this.href);}
return this._sheetAllowed;},get ruleCount(){return this._ruleCount>-1?this._ruleCount:this.domSheet.cssRules.length;},getRule:function(domRule){let cacheId=domRule.type+domRule.selectorText;let rule=null;let ruleFound=false;if(cacheId in this._rules){for(let i=0,rulesLen=this._rules[cacheId].length;i<rulesLen;i++){rule=this._rules[cacheId][i];if(rule.domRule===domRule){ruleFound=true;break;}}}
if(!ruleFound){if(!(cacheId in this._rules)){this._rules[cacheId]=[];}
rule=new CssRule(this,domRule);this._rules[cacheId].push(rule);}
return rule;},forEachRule:function(callback,scope){let ruleCount=0;let domRules=this.domSheet.cssRules;function _iterator(domRule){if(domRule.type==Ci.nsIDOMCSSRule.STYLE_RULE){callback.call(scope,this.getRule(domRule));ruleCount++;}else if(domRule.type==Ci.nsIDOMCSSRule.MEDIA_RULE&&domRule.cssRules&&this._cssLogic.mediaMatches(domRule)){Array.prototype.forEach.call(domRule.cssRules,_iterator,this);}}
Array.prototype.forEach.call(domRules,_iterator,this);this._ruleCount=ruleCount;},forSomeRules:function(callback,scope){let domRules=this.domSheet.cssRules;function _iterator(domRule){if(domRule.type==Ci.nsIDOMCSSRule.STYLE_RULE){return callback.call(scope,this.getRule(domRule));}else if(domRule.type==Ci.nsIDOMCSSRule.MEDIA_RULE&&domRule.cssRules&&this._cssLogic.mediaMatches(domRule)){return Array.prototype.some.call(domRule.cssRules,_iterator,this);}
return false;}
return Array.prototype.some.call(domRules,_iterator,this);},toString:function(){return"CssSheet["+this.shortSource+"]";}};function CssRule(cssSheet,domRule,element){this._cssSheet=cssSheet;this.domRule=domRule;let parentRule=domRule.parentRule;if(parentRule&&parentRule.type==Ci.nsIDOMCSSRule.MEDIA_RULE){this.mediaText=parentRule.media.mediaText;}
if(this._cssSheet){ this._selectors=null;this.line=domUtils.getRuleLine(this.domRule);this.source=this._cssSheet.shortSource+":"+this.line;if(this.mediaText){this.source+=" @media "+this.mediaText;}
this.href=this._cssSheet.href;this.contentRule=this._cssSheet.contentSheet;}else if(element){this._selectors=[new CssSelector(this,"@element.style",0)];this.line=-1;this.source=CssLogic.l10n("rule.sourceElement");this.href="#";this.contentRule=true;this.sourceElement=element;}}
CssRule.prototype={_passId:null,mediaText:"",get isMediaRule(){return!!this.mediaText;},get sheetAllowed(){return this._cssSheet?this._cssSheet.sheetAllowed:true;},get sheetIndex(){return this._cssSheet?this._cssSheet.index:0;},getPropertyValue:function(property){return this.domRule.style.getPropertyValue(property);},getPropertyPriority:function(property){return this.domRule.style.getPropertyPriority(property);},get selectors(){if(this._selectors){return this._selectors;}
this._selectors=[];if(!this.domRule.selectorText){return this._selectors;}
let selectors=CssLogic.getSelectors(this.domRule);for(let i=0,len=selectors.length;i<len;i++){this._selectors.push(new CssSelector(this,selectors[i],i));}
return this._selectors;},toString:function(){return"[CssRule "+this.domRule.selectorText+"]";},};function CssSelector(cssRule,selector,index){this.cssRule=cssRule;this.text=selector;this.elementStyle=this.text=="@element.style";this._specificity=null;this.selectorIndex=index;}
exports.CssSelector=CssSelector;CssSelector.prototype={_matchId:null,get source(){return this.cssRule.source;},get sourceElement(){return this.cssRule.sourceElement;},get href(){return this.cssRule.href;},get contentRule(){return this.cssRule.contentRule;},get sheetAllowed(){return this.cssRule.sheetAllowed;},get sheetIndex(){return this.cssRule.sheetIndex;},get ruleLine(){return this.cssRule.line;},get specificity(){if(this.elementStyle){


 return 0x01000000;}
if(this._specificity){return this._specificity;}
this._specificity=domUtils.getSpecificity(this.cssRule.domRule,this.selectorIndex);return this._specificity;},toString:function(){return this.text;},};function CssPropertyInfo(cssLogic,property){this._cssLogic=cssLogic;this.property=property;this._value="";this._matchedRuleCount=0;


this._matchedSelectors=null;}
CssPropertyInfo.prototype={get value(){if(!this._value&&this._cssLogic.computedStyle){try{this._value=this._cssLogic.computedStyle.getPropertyValue(this.property);}catch(ex){Services.console.logStringMessage("Error reading computed style for "+
this.property);Services.console.logStringMessage(ex);}}
return this._value;},get matchedRuleCount(){if(!this._matchedSelectors){this._findMatchedSelectors();}else if(this.needRefilter){this._refilterSelectors();}
return this._matchedRuleCount;},get matchedSelectors(){if(!this._matchedSelectors){this._findMatchedSelectors();}else if(this.needRefilter){this._refilterSelectors();}
return this._matchedSelectors;},_findMatchedSelectors:function(){this._matchedSelectors=[];this._matchedRuleCount=0;this.needRefilter=false;this._cssLogic.processMatchedSelectors(this._processMatchedSelector,this);this._matchedSelectors.sort(function(selectorInfo1,selectorInfo2){if(selectorInfo1.status>selectorInfo2.status){return-1;}else if(selectorInfo2.status>selectorInfo1.status){return 1;}
return selectorInfo1.compareTo(selectorInfo2);});if(this._matchedSelectors.length>0&&this._matchedSelectors[0].status>CssLogic.STATUS.UNMATCHED){this._matchedSelectors[0].status=CssLogic.STATUS.BEST;}},_processMatchedSelector:function(selector,status){let cssRule=selector.cssRule;let value=cssRule.getPropertyValue(this.property);if(value&&(status==CssLogic.STATUS.MATCHED||(status==CssLogic.STATUS.PARENT_MATCH&&domUtils.isInheritedProperty(this.property)))){let selectorInfo=new CssSelectorInfo(selector,this.property,value,status);this._matchedSelectors.push(selectorInfo);if(this._cssLogic._passId!==cssRule._passId&&cssRule.sheetAllowed){this._matchedRuleCount++;}}},_refilterSelectors:function(){let passId=++this._cssLogic._passId;let ruleCount=0;let iterator=function(selectorInfo){let cssRule=selectorInfo.selector.cssRule;if(cssRule._passId!=passId){if(cssRule.sheetAllowed){ruleCount++;}
cssRule._passId=passId;}};if(this._matchedSelectors){this._matchedSelectors.forEach(iterator);this._matchedRuleCount=ruleCount;}
this.needRefilter=false;},toString:function(){return"CssPropertyInfo["+this.property+"]";},};function CssSelectorInfo(selector,property,value,status){this.selector=selector;this.property=property;this.status=status;this.value=value;let priority=this.selector.cssRule.getPropertyPriority(this.property);this.important=(priority==="important");}
CssSelectorInfo.prototype={get source(){return this.selector.source;},get sourceElement(){return this.selector.sourceElement;},get href(){return this.selector.href;},get elementStyle(){return this.selector.elementStyle;},get specificity(){return this.selector.specificity;},get sheetIndex(){return this.selector.sheetIndex;},get sheetAllowed(){return this.selector.sheetAllowed;},get ruleLine(){return this.selector.ruleLine;},get contentRule(){return this.selector.contentRule;},compareTo:function(that){if(!this.contentRule&&that.contentRule){return 1;}
if(this.contentRule&&!that.contentRule){return-1;}
if(this.elementStyle&&!that.elementStyle){if(!this.important&&that.important){return 1;}
return-1;}
if(!this.elementStyle&&that.elementStyle){if(this.important&&!that.important){return-1;}
return 1;}
if(this.important&&!that.important){return-1;}
if(that.important&&!this.important){return 1;}
if(this.specificity>that.specificity){return-1;}
if(that.specificity>this.specificity){return 1;}
if(this.sheetIndex>that.sheetIndex){return-1;}
if(that.sheetIndex>this.sheetIndex){return 1;}
if(this.ruleLine>that.ruleLine){return-1;}
if(that.ruleLine>this.ruleLine){return 1;}
return 0;},toString:function(){return this.selector+" -> "+this.value;},};DevToolsUtils.defineLazyGetter(this,"domUtils",function(){return Cc["@mozilla.org/inspector/dom-utils;1"].getService(Ci.inIDOMUtils);});