"use strict";
module.metadata={"stability":"experimental"};const{Ci}=require("chrome");const{windows,isInteractive}=require("../window/utils");const{events}=require("../browser/events");const{open}=require("../event/dom");const{filter,map,merge,expand}=require("../event/utils");const isFennec=require("sdk/system/xul-app").is("Fennec");



const TYPES=["TabOpen","TabClose","TabSelect","TabMove","TabPinned","TabUnpinned"];
function tabEventsFor(window){

let channels=TYPES.map(type=>open(window,type));return merge(channels);}
var readyEvents=filter(events,e=>e.type==="DOMContentLoaded");var futureWindows=map(readyEvents,e=>e.target);

var eventsFromFuture=expand(futureWindows,tabEventsFor);

var interactiveWindows=windows("navigator:browser",{includePrivate:true}).filter(isInteractive);var eventsFromInteractive=merge(interactiveWindows.map(tabEventsFor));
var allEvents=merge([eventsFromInteractive,eventsFromFuture]);exports.events=map(allEvents,function(event){return!isFennec?event:{type:event.type,target:event.target.ownerDocument.defaultView.BrowserApp.getTabForBrowser(event.target)};});