"use strict";this.EXPORTED_SYMBOLS=["LoginHelper",];const{classes:Cc,interfaces:Ci,utils:Cu,results:Cr}=Components;Cu.import("resource://gre/modules/Services.jsm");Cu.import("resource://gre/modules/XPCOMUtils.jsm"); this.LoginHelper={debug:Services.prefs.getBoolPref("signon.debug"),createLogger(aLogPrefix){let getMaxLogLevel=()=>{return this.debug?"debug":"error";};let ConsoleAPI=Cu.import("resource://gre/modules/Console.jsm",{}).ConsoleAPI;let consoleOptions={maxLogLevel:getMaxLogLevel(),prefix:aLogPrefix,};let logger=new ConsoleAPI(consoleOptions); Services.prefs.addObserver("signon.",()=>{this.debug=Services.prefs.getBoolPref("signon.debug");logger.maxLogLevel=getMaxLogLevel();},false);return logger;},checkHostnameValue:function(aHostname)
{

if(aHostname=="."||aHostname.indexOf("\r")!=-1||aHostname.indexOf("\n")!=-1||aHostname.indexOf("\0")!=-1){throw new Error("Invalid hostname");}},checkLoginValues:function(aLogin)
{function badCharacterPresent(l,c)
{return((l.formSubmitURL&&l.formSubmitURL.indexOf(c)!=-1)||(l.httpRealm&&l.httpRealm.indexOf(c)!=-1)||l.hostname.indexOf(c)!=-1||l.usernameField.indexOf(c)!=-1||l.passwordField.indexOf(c)!=-1);}
if(badCharacterPresent(aLogin,"\0")){throw new Error("login values can't contain nulls");}



if(aLogin.username.indexOf("\0")!=-1||aLogin.password.indexOf("\0")!=-1){throw new Error("login values can't contain nulls");}
if(badCharacterPresent(aLogin,"\r")||badCharacterPresent(aLogin,"\n")){throw new Error("login values can't contain newlines");}
if(aLogin.usernameField=="."||aLogin.formSubmitURL=="."){throw new Error("login values can't be periods");}
if(aLogin.hostname.indexOf(" (")!=-1){throw new Error("bad parens in hostname");}},buildModifiedLogin:function(aOldStoredLogin,aNewLoginData)
{function bagHasProperty(aPropName)
{try{aNewLoginData.getProperty(aPropName);return true;}catch(ex){}
return false;}
aOldStoredLogin.QueryInterface(Ci.nsILoginMetaInfo);let newLogin;if(aNewLoginData instanceof Ci.nsILoginInfo){
newLogin=aOldStoredLogin.clone();newLogin.init(aNewLoginData.hostname,aNewLoginData.formSubmitURL,aNewLoginData.httpRealm,aNewLoginData.username,aNewLoginData.password,aNewLoginData.usernameField,aNewLoginData.passwordField);newLogin.QueryInterface(Ci.nsILoginMetaInfo);if(newLogin.password!=aOldStoredLogin.password){newLogin.timePasswordChanged=Date.now();}}else if(aNewLoginData instanceof Ci.nsIPropertyBag){newLogin=aOldStoredLogin.clone();newLogin.QueryInterface(Ci.nsILoginMetaInfo);
if(bagHasProperty("password")){let newPassword=aNewLoginData.getProperty("password");if(newPassword!=aOldStoredLogin.password){newLogin.timePasswordChanged=Date.now();}}
let propEnum=aNewLoginData.enumerator;while(propEnum.hasMoreElements()){let prop=propEnum.getNext().QueryInterface(Ci.nsIProperty);switch(prop.name){ case"hostname":case"httpRealm":case"formSubmitURL":case"username":case"password":case"usernameField":case"passwordField": case"guid":case"timeCreated":case"timeLastUsed":case"timePasswordChanged":case"timesUsed":newLogin[prop.name]=prop.value;break;case"timesUsedIncrement":newLogin.timesUsed+=prop.value;break;default:throw new Error("Unexpected propertybag item: "+prop.name);}}}else{throw new Error("newLoginData needs an expected interface!");} 
if(newLogin.hostname==null||newLogin.hostname.length==0){throw new Error("Can't add a login with a null or empty hostname.");}
if(newLogin.username==null){throw new Error("Can't add a login with a null username.");}
if(newLogin.password==null||newLogin.password.length==0){throw new Error("Can't add a login with a null or empty password.");}
if(newLogin.formSubmitURL||newLogin.formSubmitURL==""){if(newLogin.httpRealm!=null){throw new Error("Can't add a login with both a httpRealm and formSubmitURL.");}}else if(newLogin.httpRealm){if(newLogin.formSubmitURL!=null){throw new Error("Can't add a login with both a httpRealm and formSubmitURL.");}}else{throw new Error("Can't add a login without a httpRealm or formSubmitURL.");}
this.checkLoginValues(newLogin);return newLogin;},dedupeLogins(logins,uniqueKeys=["username","password"]){const KEY_DELIMITER=":";function getKey(login,uniqueKeys){return uniqueKeys.reduce((prev,key)=>prev+KEY_DELIMITER+login[key],"");}
let loginsByKeys=new Map();for(let login of logins){let key=getKey(login,uniqueKeys);if(loginsByKeys.has(key)){let loginDate=login.QueryInterface(Ci.nsILoginMetaInfo).timeLastUsed;let storedLoginDate=loginsByKeys.get(key).QueryInterface(Ci.nsILoginMetaInfo).timeLastUsed;if(loginDate<storedLoginDate){continue;}}
loginsByKeys.set(key,login);}
return[...loginsByKeys.values()];},openPasswordManager(window,filterString=""){let win=Services.wm.getMostRecentWindow("Toolkit:PasswordManager");if(win){win.setFilter(filterString);win.focus();}else{window.openDialog("chrome://passwordmgr/content/passwordManager.xul","Toolkit:PasswordManager","",{filterString:filterString});}},isUsernameFieldType(element){if(!(element instanceof Ci.nsIDOMHTMLInputElement))
return false;let fieldType=(element.hasAttribute("type")?element.getAttribute("type").toLowerCase():element.type);if(fieldType=="text"||fieldType=="email"||fieldType=="url"||fieldType=="tel"||fieldType=="number"){return true;}
return false;},maybeImportLogin(loginData){ let login=Cc["@mozilla.org/login-manager/loginInfo;1"].createInstance(Ci.nsILoginInfo);login.init(loginData.hostname,loginData.submitURL||(typeof(loginData.httpRealm)=="string"?null:""),typeof(loginData.httpRealm)=="string"?loginData.httpRealm:null,loginData.username,loginData.password,loginData.usernameElement||"",loginData.passwordElement||"");login.QueryInterface(Ci.nsILoginMetaInfo);login.timeCreated=loginData.timeCreated;login.timeLastUsed=loginData.timeLastUsed||loginData.timeCreated;login.timePasswordChanged=loginData.timePasswordChanged||loginData.timeCreated;login.timesUsed=loginData.timesUsed||1;
let existingLogins=Services.logins.findLogins({},login.hostname,login.formSubmitURL,login.httpRealm);

 if(existingLogins.some(l=>login.matches(l,true))){return;} 
let foundMatchingLogin=false;for(let existingLogin of existingLogins){if(login.username==existingLogin.username){ foundMatchingLogin=true;if(login.password!=existingLogin.password&login.timePasswordChanged>existingLogin.timePasswordChanged){

 let propBag=Cc["@mozilla.org/hash-property-bag;1"].createInstance(Ci.nsIWritablePropertyBag);propBag.setProperty("password",login.password);propBag.setProperty("timePasswordChanged",login.timePasswordChanged);Services.logins.modifyLogin(existingLogin,propBag);}}}
if(foundMatchingLogin){return;}
Services.logins.addLogin(login);},loginsToVanillaObjects(logins){return logins.map(this.loginToVanillaObject);},loginToVanillaObject(login){let obj={};for(let i in login){if(typeof login[i]!=='function'){obj[i]=login[i];}}
login.QueryInterface(Ci.nsILoginMetaInfo);obj.guid=login.guid;return obj;},vanillaObjectToLogin(login){var formLogin=Cc["@mozilla.org/login-manager/loginInfo;1"].createInstance(Ci.nsILoginInfo);formLogin.init(login.hostname,login.formSubmitURL,login.httpRealm,login.username,login.password,login.usernameField,login.passwordField);formLogin.QueryInterface(Ci.nsILoginMetaInfo);formLogin.guid=login.guid;return formLogin;},vanillaObjectsToLogins(logins){return logins.map(this.vanillaObjectToLogin);}};